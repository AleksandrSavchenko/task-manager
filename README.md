# Project Overview #

Task manager - application for an asynchronous compute the hash for external / internal files.

### Technology stack ###

* Java 8
* Maven 3.1
* Spring 4.2.5 (Core, Data, MVC, RESTapi)
* HSQLDB(in memory)
* AngularJS
* jUnit/dbUnit/EasyMock!

### Application UI ###
[taskManager.png](https://bitbucket.org/repo/AqX75p/images/4177593445-taskManager.png)