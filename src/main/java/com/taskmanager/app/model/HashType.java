package com.taskmanager.app.model;

/**
 * Type for hashing algorithm
 */
public enum HashType {
    /**
     * SHA-1
     */
    SHA_1,
    /**
     * SHA-256
     */
    SHA_256,
    /**
     * MD-5
     */
    MD5
}
