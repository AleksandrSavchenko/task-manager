package com.taskmanager.app.core.agents.api;

/**
 * Agent interface
 */
public interface Agent {

    /**
     * Execute agent
     */
    void execute();
}
